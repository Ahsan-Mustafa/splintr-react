import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import './App.css';
import HomeView from './views/home.jsx';
import SignInView from './views/register/signin.jsx';
import Dashmain from './views/dashboard/main.jsx';

function App() {
  return (
    <BrowserRouter>
      <React.Fragment>
        <Switch>
          <Route exact path="/" component={HomeView} />
          <Route exact path="/signin" component={SignInView} />
          <Route path="/dashmain" component={Dashmain}/>
        </Switch>
      </React.Fragment>
    </BrowserRouter>
  );
}

export default App;
