import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';


export default function Helpmain() {

    const useStyles = makeStyles((theme) => ({
        pd10: {
            paddingLeft:'10px',
            paddingRight: '10px'
        },
        noPadd: {
            paddingLeft: "0px",
            paddingRight: "0px",
            padding: "0px"
        },
        heading: {
            backgroundColor: '#47d9ed',
            color: '#fff',
            fontWeight: 'bold',
            '& h3': {
                padding: '15px',
                fontWeight: 'bold'
            }
        }
	}));

	const classes = useStyles();

    return (
        <Box mb={5}>
            <Card>
                <CardContent className={classes.heading}>
                    <Typography variant="h3">
                        Help
                    </Typography>
                </CardContent>
            </Card>
        </Box>
    );
}
