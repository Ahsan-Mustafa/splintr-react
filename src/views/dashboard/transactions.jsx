import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Chart from '../../components/dashboard/chart';
import Transactiontabs from '../../components/dashboard/transactiontabs';
import Purchasetabs from '../../components/dashboard/purchasetabs';

export default function Transactionmain() {

    const useStyles = makeStyles((theme) => ({
        pd10: {
            paddingLeft:'10px',
            paddingRight: '10px'
        },
        noPadd: {
            paddingLeft: "0px",
            paddingRight: "0px",
            padding: "0px"
        },
        heading: {
            backgroundColor: '#47d9ed',
            color: '#fff',
            fontWeight: 'bold',
            '& h3': {
                padding: '15px',
                fontWeight: 'bold'
            }
        }
	}));

	const classes = useStyles();

    return (
        <Box>
            <Box mb={5}>
                <Card>
                    <CardContent className={classes.heading}>
                        <Typography variant="h3" >
                            Your Transactions
                        </Typography>
                    </CardContent>
                </Card>
            </Box>
            <Grid container>
                <Grid item xs={12} sm={6} md={6} lg={6} xl={6} className={classes.pd10}>
                    <Box mb={5}>
                        <Card>
                            <CardContent>
                                <Chart/>
                            </CardContent>
                        </Card>
                    </Box>
                    <Box mb={5}>
                        <Card>
                            <CardContent className={classes.noPadd}>
                                <Transactiontabs/>
                            </CardContent>
                        </Card>
                    </Box>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6} xl={6} className={classes.pd10}>
                    <Box mb={5}>
                        <Card>
                            <CardContent>
                                <Purchasetabs/>
                            </CardContent>
                        </Card>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    );
}
