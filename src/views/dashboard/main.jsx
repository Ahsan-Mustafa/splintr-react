import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import Headerbar from '../../components/dashboard/header';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {
    Switch,
    Route,
    useRouteMatch
  } from "react-router-dom";
import Link from '@material-ui/core/Link';
import Transactionmain from './transactions.jsx';
import Profilemain from './profile.jsx';
import Paymentmain from './payment.jsx';
import Legalmain from './legal.jsx';
import Helpmain from './help.jsx';

export default function Dashmain() {

    const useStyles = makeStyles((theme) => ({
        dashbg: { backgroundColor: '#ecf4fa' },
        pd10: { paddingLeft:'10px', paddingRight: '10px'},
        icons: { maxWidth: "25px"},
        noPadd: { paddingLeft: "0px", paddingRight: "0px", padding: "0px"}
	}));

	const classes = useStyles();
    let { path, url } = useRouteMatch();
    return (
        <React.Fragment>
            <CssBaseline />
            <Headerbar />
            <Box className={classes.dashbg} pt={7} pb={7} pr={2} pl={2}>
            <Container maxWidth="xl">
                <Grid container>
        			<Grid item xs={12} sm={12} md={12} lg={3} xl={2} className={classes.pd10}>
                        <Card>
                            <CardContent>
                                <List component="nav" aria-label="main mailbox folders">
                                    <Link href={`${url}/profile`}>
                                        <ListItem button>
                                            <ListItemIcon>
                                                <img className={classes.icons} src="https://res.cloudinary.com/djpepozcx/image/upload/v1614731677/splintr/icon-4.png" alt="profile"/>
                                            </ListItemIcon>
                                            <ListItemText primary="My Profile" />
                                        </ListItem>
                                    </Link>
                                    <Link href={`${url}/transactions`}>
                                        <ListItem button>
                                            <ListItemIcon>
                                                <img className={classes.icons} src="https://res.cloudinary.com/djpepozcx/image/upload/v1614731677/splintr/icon-2.png" alt="transactions"/>
                                            </ListItemIcon>
                                            <ListItemText primary="Your Transactions" />
                                        </ListItem>
                                    </Link>
                                    <Link href={`${url}/payment`}>
                                        <ListItem button>
                                            <ListItemIcon>
                                                <img className={classes.icons} src="https://res.cloudinary.com/djpepozcx/image/upload/v1614731677/splintr/icon-1.png" alt="transactions"/>
                                            </ListItemIcon>
                                            <ListItemText primary="Payment Method" />
                                        </ListItem>
                                    </Link>
                                    <Link href={`${url}/Legal`}>
                                        <ListItem button>
                                            <ListItemIcon>
                                                <img className={classes.icons} src="https://res.cloudinary.com/djpepozcx/image/upload/v1614731677/splintr/icon-0.png" alt="transactions"/>
                                            </ListItemIcon>
                                            <ListItemText primary="Legal" />
                                        </ListItem>
                                    </Link>
                                    <Link href={`${url}/help`}>
                                        <ListItem button>
                                            <ListItemIcon>
                                                <img className={classes.icons} src="https://res.cloudinary.com/djpepozcx/image/upload/v1614731677/splintr/icon-3.png" alt="transactions"/>
                                            </ListItemIcon>
                                            <ListItemText primary="Help Center" />
                                        </ListItem>
                                    </Link>
                                </List>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={9} xl={10} className={classes.pd10}>
                        <Switch>
                            <Route path={`${path}/profile`}>
                                <Profilemain/>
                            </Route>
                            <Route path={`${path}/transactions`}>
                                <Transactionmain/>
                            </Route>
                            <Route path={`${path}/payment`}>
                                <Paymentmain/>
                            </Route>
                            <Route path={`${path}/legal`}>
                                <Legalmain/>
                            </Route>
                            <Route path={`${path}/help`}>
                                <Helpmain/>
                            </Route>
                        </Switch>
                    </Grid>
                </Grid>
            </Container>
            </Box>
        </React.Fragment>
    );
}
