import React from "react";
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Footer from '../../components/footer';
import Link from '@material-ui/core/Link';

const useStyles = (theme) => ({
	logoblock: { margin: '50px 100px 50px 100px',
		'& img': { maxWidth: '120px' },
		"@media (max-width: 768px)": { margin: '25px 30px 25px 30px',},
	},
	  image: {
		backgroundImage: 'url(https://res.cloudinary.com/djpepozcx/image/upload/v1614731652/splintr/login-screen.jpg)',
		backgroundRepeat: 'no-repeat',
		backgroundColor: theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
		backgroundSize: 'cover',
		backgroundPosition: 'top',
	  },
	  paper: {
		margin: '100px', display: 'flex', flexDirection: 'column',
		"@media (max-width: 768px)": { margin: "30px" }
	  },
	typopaper: {
		margin: '100px 100px 200px 100px', '& span': { color: '#adadad' },
		"@media (max-width: 768px)": { margin: "50px 30px 100px 30px" }
	},
	  form: { width: '100%'},
	  submit: { margin: theme.spacing(3, 0, 2), float: 'right', minWidth: '220px', textTransform: 'capitalize',
		backgroundColor: '#000', color: '#fff', borderRadius: '50px',
		'& span': { '& a': { color: '#fff', textDecoration: 'none' } },
		'&:hover': { backgroundColor: '#000', color: '#fff' },
		"@media (max-width: 768px)": { minWidth: '100%',}
	  },
	heading: { fontSize: '54px', fontWeight: 'bold', marginBottom: '100px',
		"@media (max-width: 768px)": { fontSize: '24px', marginBottom: '50px'}
	},
	text: { textAlign: 'center',fontSize: '18px', fontWeight: 'normal', margin: '7px'},
	inputradius: { marginTop: '8px', '& div': { borderRadius: '30px' } },
	errorMsg: {
		color: "#cc0000",
		marginBottom: "12px"
	}
});

class SigninView extends React.Component {

	constructor() {
		super();
		this.state = {
		  fields: {},
		  errors: {}
		}
  
		this.handleChange = this.handleChange.bind(this);
		this.loginform = this.loginform.bind(this);
  
	}

	handleChange(e) {
		let fields = this.state.fields;
		fields[e.target.name] = e.target.value;
		this.setState({
		  fields
		});
  
	  }
  
	  loginform(e) {
		e.preventDefault();
		let obj = this.state.fields;
		obj.token = (Math.random() * (100 - 1));
		localStorage.setItem('user', JSON.stringify(obj));
		if (this.validateForm()) {
			let fields = {};
			fields["emailid"] = "";
			fields["mobileno"] = "";
			this.setState({fields:fields});
			alert("Form submitted");
		}
  
	  }

	  validateForm() {

		let fields = this.state.fields;
		let errors = {};
		let formIsValid = true;
  
  
		if (!fields["emailid"]) {
		  formIsValid = false;
		  errors["emailid"] = "*Please enter your email-ID.";
		}
  
		if (typeof fields["emailid"] !== "undefined") {
		  var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		  if (!pattern.test(fields["emailid"])) {
			formIsValid = false;
			errors["emailid"] = "*Please enter valid email-ID.";
		  }
		}
  
		if (!fields["mobileno"]) {
		  formIsValid = false;
		  errors["mobileno"] = "*Please enter your mobile no.";
		}
  
		if (typeof fields["mobileno"] !== "undefined") {
		  if (!fields["mobileno"].match(/^[0-9]{10}$/)) {
			formIsValid = false;
			errors["mobileno"] = "*Please enter valid mobile no.";
		  }
		}
  
		this.setState({
		  errors: errors
		});
		return formIsValid;
	  }

	render() {
		const { classes } = this.props;
		return(
			<Grid container component="main">
				<CssBaseline />
				<Grid item xs={12} sm={8} md={6} component={Paper} elevation={6} square>
					<div className={classes.logoblock}>
						<Link href="/">
							<img src="https://res.cloudinary.com/djpepozcx/image/upload/v1614731649/splintr/logo.svg" alt="logo"/>
						</Link>
					</div>
					<div className={classes.paper}>
						<Typography component="h1" className={classes.heading}>
							Sign in
						  </Typography>
						  <form method="post" name="loginform" onSubmit= {this.loginform} className={classes.form}>
							<TextField className={classes.inputradius} variant="outlined" 
								margin="normal" fullWidth id="email" label="Email" 
								autoComplete="email" autoFocus  name="emailid" value={this.state.fields.emailid || ''} onChange={this.handleChange} />
							<div className={classes.errorMsg}>{this.state.errors.emailid}</div>
							<Typography className={classes.text}>
								OR
							</Typography>
							<TextField className={classes.inputradius} variant="outlined" margin="normal" fullWidth name="mobileno" value={this.state.fields.mobileno || ''} onChange={this.handleChange} 
								label="Phone Number" type="number" id="phone"/>
							<div className={classes.errorMsg}>{this.state.errors.mobileno}</div>
							<Button type="submit" variant="contained"color="primary" className={classes.submit}> 
								<Link href="/Dashmain">
									Sign In
								</Link>
							</Button>
						</form>
					</div>
					<div className={classes.typopaper}>
						<Typography variant="caption" display="block" gutterBottom>
							Message and data rates may apply.
						  </Typography>
						<Typography variant="caption" display="block" gutterBottom>
							By Proceeding, I accept Splintr and confirm that i have read Splintr's Privacy Notice
						</Typography>
						<Typography variant="caption" display="block" gutterBottom>
							This page is protected by reCAPTCHA. By continuing I confirm having read Google's Privacy Policy and accepted Google's Terms.
						</Typography>
					</div>
				</Grid>
				<Grid item xs={false} sm={4} md={6} className={classes.image} />
				<Footer />
			</Grid>
		)
	}
}


export default withStyles(useStyles)(SigninView);