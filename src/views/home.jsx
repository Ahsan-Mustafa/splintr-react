import React, { Component } from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import Header from '../components/header.jsx';
import Footer from '../components/footer.jsx';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Blocks from '../components/blocks';
import Mobileblock from '../components/mobile';
import Sliders from '../components/sliders';
import Learnmoreblock from '../components/learnmore';
import Deals from '../components/deals';
import {best_sellers, top_deals} from '../data/data';

const useStyles = theme => ({
	banner: { maxWidth: '100%', display: 'block'},
	headerBlock: { position: 'relative',},
	bannerText: {
		position:"absolute",top: "35%",left: "9.2%",
		"& h2": {fontWeight: "bold",fontSize: "84px",marginBottom: "20px",
			"@media (max-width: 768px)": {fontSize: "18px",},
			"@media (max-width: 480px)": {display: "none"},
			"@media only screen and (min-device-width:768px) and (max-device-width:1024px)": {fontSize: "50px"}
		},
		"& span": {fontSize: "18px",fontWeight: "bold",
			"@media (max-width: 768px)": {fontSize: "11px",fontWeight: "normal"},
			"@media (max-width: 480px)": {display: "none"},
			"@media only screen and (min-device-width:768px) and (max-device-width:1024px)": {fontSize: "14px"}
		},
	},
	noPadding: {paddingLeft: "0px",paddingRight: "0px",},
	Padd5: {paddingLeft: "5px",paddingRight: "5px",},
});


class HomeView extends Component {
	render() {
		const { classes } = this.props;
		return(
			<React.Fragment>
				<CssBaseline />
				<Container maxWidth="xl" className={classes.noPadding}>
					<div className={classes.headerBlock}>
						<Header />
						<img className={classes.banner} src="https://res.cloudinary.com/djpepozcx/image/upload/v1614731652/splintr/homepage.jpg" alt="banner"/>
						<div className={classes.bannerText}>
							<Typography variant="h1" component="h2">
								Buy Now <br/> Pay Layer.
							</Typography>
							<Typography variant="caption" display="block">
								Choose splintr at checkout and pay for your purchase<br/>
								over three installments without paying any interest and<br/> 
								get your order straightway.
							</Typography>
						</div>
					</div>
					<Container maxWidth="lg">
						<Blocks/>
						<Mobileblock/>
						<Sliders title="Best Seller" data={best_sellers}/>
						<Deals title="Top Deals" data={top_deals}/>
					</Container>
					<Learnmoreblock/>
					<Footer />
				</Container>
			</React.Fragment>
		)
	}
}

export default withStyles(useStyles)(HomeView);