import React from "react";
import { DonutChart } from "bizcharts";

const data = [
  {
    type: "Pay After Delivery",
    value: 27,
  },
  {
    type: "Pay After Installments",
    value: 25,
  },
  {
    type: "Pay After Delivery",
    value: 11,
  },
  {
    type: "Pay After Installments",
    value: 22,
  },
  {
    type: "Pay After Delivery",
    value: 45,
  },
  {
    type: "Pay After Installments",
    value: 78,
  }
];

export default function Chart() {
  return (
    <DonutChart data={data || []} autoFit height={200} radius={0.8} padding="auto" angleField="value" colorField="type" pieStyle={{ stroke: "white", lineWidth: 5 }} />
  );
}
