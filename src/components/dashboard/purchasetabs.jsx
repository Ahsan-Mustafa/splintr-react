import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineDot from '@material-ui/lab/TimelineDot'; 
import Grid from '@material-ui/core/Grid';


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
    tabs: {
        "& .MuiTabs-indicator": {
            backgroundColor: "#000"
        }
    },
    textBold: {
      fontWeight: 'bold'
    },
    bar: {
        borderBottom: "2px solid",
        marginTop: "10px",
        marginBottom: "20px"
     },
    right: {
      float: "right",
      textAlign: "right"
    },
    tm: {
      '&:before':{
          flex: '0',
          content: "none"
      }
    }
}));

export default function Purchasetabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
        <Typography variant="h6" className={classes.textBold}>
            STYLI
        </Typography>
        <Typography variant="subtitle1">
            Purchase Amount 250 AED.
        </Typography>
        <div className={classes.bar}>
            <Typography variant="caption" className={classes.textBold}>Remaining Amount</Typography>
            <Typography variant="caption"  className={`${classes.textBold} ${classes.right}`}>250 AED</Typography>
        </div>
        <Tabs className={classes.tabs} value={value} onChange={handleChange} aria-label="simple tabs example">
          <Tab label="Schedule" {...a11yProps(0)} />
          <Tab label="Details" {...a11yProps(1)} />
        </Tabs>
        <TabPanel value={value} index={0}>
            <Timeline>
                <TimelineItem className={classes.tm}>
                    <TimelineSeparator>
                        <TimelineDot color="secondary"/>
                        <TimelineConnector />
                    </TimelineSeparator>
                    <TimelineContent>
                        <Grid container>
                            <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                                <Typography variant="subtitle1">Noon</Typography> 
                                <Typography variant="caption">30 October 2020</Typography> 
                            </Grid>
                            <Grid item xs={12} sm={6} md={6} lg={6} xl={6} className={classes.right}>
                                <Typography variant="subtitle1">AED 28.00</Typography> 
                                <Typography variant="caption">Paid</Typography> 
                            </Grid>
                        </Grid>

                    </TimelineContent>
                </TimelineItem>
        
                <TimelineItem className={classes.tm}>
                    <TimelineSeparator>
                        <TimelineDot />
                        <TimelineConnector />
                    </TimelineSeparator>
                    <TimelineContent>
                        <Grid container>
                            <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                                <Typography variant="subtitle1">Noon</Typography> 
                                <Typography variant="caption">30 October 2020</Typography> 
                            </Grid>
                            <Grid item xs={12} sm={6} md={6} lg={6} xl={6} className={classes.right}>
                                <Typography variant="subtitle1">AED 28.00</Typography> 
                                <Typography variant="caption">Paid</Typography> 
                            </Grid>
                        </Grid>
                    </TimelineContent>
                </TimelineItem>
        
                <TimelineItem className={classes.tm}>
                    <TimelineSeparator>
                        <TimelineDot />
                        <TimelineConnector />
                    </TimelineSeparator>
                    <TimelineContent>
                        <Grid container>
                            <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                                <Typography variant="subtitle1">Noon</Typography> 
                                <Typography variant="caption">30 October 2020</Typography> 
                            </Grid>
                            <Grid item xs={12} sm={6} md={6} lg={6} xl={6} className={classes.right}>
                                <Typography variant="subtitle1">AED 28.00</Typography> 
                                <Typography variant="caption">Paid</Typography> 
                            </Grid>
                        </Grid>
                    </TimelineContent>
                </TimelineItem>
        
                <TimelineItem className={classes.tm}>
                    <TimelineSeparator>
                        <TimelineDot color="secondary"/>
                    </TimelineSeparator>
                    <TimelineContent>
                    <Grid container>
                        <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                            <Typography variant="subtitle1">Noon</Typography> 
                            <Typography variant="caption">30 October 2020</Typography> 
                        </Grid>
                        <Grid item xs={12} sm={6} md={6} lg={6} xl={6} className={classes.right}>
                            <Typography variant="subtitle1">AED 28.00</Typography> 
                            <Typography variant="caption">Paid</Typography> 
                        </Grid>
                    </Grid>
                    </TimelineContent>
                </TimelineItem>
            </Timeline>
        </TabPanel>
      <TabPanel value={value} index={1}>
          Details
      </TabPanel>
    </div>
  );
}
