import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,

  },
  tabs: {
      borderBottom: "1px solid",
      justifyContent: "space-evenly",
      "& button": {
          width: "100%",
      },
      "& .Mui-selected": {
        backgroundColor: "#47d9ed"
      },
      "& .MuiTabs-indicator": {
        backgroundColor: "#47d9ed"
      }
  },
  textBold: {
      fontWeight: 'bold'
  }
}));

export default function Transactiontabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
        <Tabs className={classes.tabs} value={value} onChange={handleChange} aria-label="simple tabs example">
          <Tab label="Current" {...a11yProps(0)} />
          <Tab label="Completed" {...a11yProps(1)} />
        </Tabs>

        <TabPanel value={value} index={0}>
            <List className={classes.root}>
                <ListItem>
                    <ListItemText variant="h6" secondary={
                        <React.Fragment>
                            <Typography className={classes.textBold} variant="subtitle1" color="textPrimary">
                                Noon 
                            </Typography>    
                            <Typography variant="subtitle1" color="textPrimary">
                                Next Repayment 270 AED 
                            </Typography>
                            <Typography variant="subtitle2">
                                Due: 30 October 2020
                            </Typography>  
                        </React.Fragment>
                        }/>
                        <ListItemSecondaryAction>
                            <Button variant="contained" color="secondary" edge="end">Pay Now</Button>
                        </ListItemSecondaryAction>
                </ListItem>
                <ListItem>
                    <ListItemText secondary={
                        <React.Fragment>
                            <Typography className={classes.textBold} variant="subtitle1" color="textPrimary">
                                Styli
                            </Typography>    
                            <Typography variant="subtitle1" color="textPrimary">
                                Next Repayment 270 AED 
                            </Typography>
                            <Typography variant="subtitle2">
                                Due: 26 October 2020
                            </Typography>  
                        </React.Fragment>
                        }/>
                    <ListItemSecondaryAction>
                        <Button variant="contained" color="secondary" edge="end">Pay Now</Button>
                    </ListItemSecondaryAction>
                </ListItem>
                <ListItem>
                    <ListItemText secondary={
                        <React.Fragment>
                            <Typography className={classes.textBold} variant="subtitle1" color="textPrimary">
                                Ounass
                            </Typography>    
                            <Typography variant="subtitle1" color="textPrimary">
                                Next Repayment 270 AED 
                            </Typography>
                            <Typography variant="subtitle2">
                                Due: 16 October 2020
                            </Typography>
                            <Typography variant="caption">
                                Pay after deliver plan
                            </Typography>
                        </React.Fragment>
                    }/>
                        <ListItemSecondaryAction>
                            <Button color="secondary" variant="contained" edge="end">Pay Now</Button>
                        </ListItemSecondaryAction>
                </ListItem>

            </List>
        </TabPanel>
      <TabPanel value={value} index={1}>
        Completed
      </TabPanel>
    </div>
  );
}
