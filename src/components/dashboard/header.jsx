import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Container from '@material-ui/core/Container';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  root: { flexGrow: 1 },
  menuButton: { marginRight: theme.spacing(1) },
  loginBtn: { marginLeft: 'auto' },
  whitebar: { backgroundColor: '#fff', color: '#000', boxShadow: 'none', position: 'relative' },
  selectborder: { border: "1px solid #dedede", padding: "5px" }
}));

export default function Headerbar() {
  const classes = useStyles();
  const [age] = React.useState('profile');

  return (
    <div className={classes.root}>
      <AppBar className={classes.whitebar}>
        <Container maxWidth="xl">
          <Toolbar className={classes.toolbar}>
            <img className={classes.title} src="https://res.cloudinary.com/djpepozcx/image/upload/v1614731649/splintr/logo.svg" alt="logo"/>
            <div className={classes.loginBtn}>
              <FormControl className={classes.formControl}>
                <Select className={classes.selectborder} labelId="demo-simple-select-label" id="demo-simple-select" value={age}>
                  <MenuItem value={'profile'}>Muhammad Akram</MenuItem>
                  <MenuItem value={'logout'}>Logout</MenuItem>
                </Select>
              </FormControl>
            </div>
          </Toolbar>
        </Container>
      </AppBar>
    </div>
  );
}
