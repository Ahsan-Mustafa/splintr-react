import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Link from '@material-ui/core/Link';

function ListItemLink(props) {
    return <ListItem button component="a" {...props} />;
}

export default function Footer() {
    const useStyles = makeStyles((theme) => ({
  	    footerblock: {
    		width: '100%',
            backgroundColor: '#000',
            color: '#fff',
            paddingLeft: '150px',
            paddingRight: '100px',
            paddingTop: '30px',
            paddingBottom: '30px',
            '& img': {
                maxWidth: '80px',
                marginTop: '40px',
                marginBottom: '20px'
            },
            '& a': {
                paddingTop: '0px',
                paddingBottom: '0px'
            },
            "@media (max-width: 768px)": {
                paddingLeft: '30px',
                paddingRight: '30px',
                paddingTop: '15px',
                paddingBottom: '15px',
                '& img': {
                    maxWidth: '50px',
                    marginTop: '20px',
                    marginBottom: '10px'
                },
                '& a': {
                    '& span': {
                        fontSize: '13px'
                    }
                },
			}
  		},
        flexContainer : {
            display: 'flex',
            flexDirection: 'row',
            float: 'right',
            '& img':{
                marginTop: "0px",
                marginBottom: "0px",
                marginLeft: "10px"
            }
        },
        navlist: {
            "@media (max-width: 768px)": {
                '& a': {
                    paddingLeft: "0px"
                }
            }
        }
	}));

	const classes = useStyles();
    return (
        <footer className={classes.footerblock}>
            <Grid container>
                <Grid item xs={12} sm={12} md={3}>
                    <img src="https://res.cloudinary.com/djpepozcx/image/upload/v1614731647/splintr/footer-symbol.svg" alt="footer"/>
                </Grid>
                <Grid item xs={6} sm={6} md={2}>
                    <List component="nav" className={classes.navlist}>
                        <ListItemLink href="/about">
                            <ListItemText primary="About Us" />
                        </ListItemLink>
                        <ListItemLink href="/careers">
                            <ListItemText primary="Careers" />
                        </ListItemLink>
                        <ListItemLink href="/privacy">
                            <ListItemText primary="Privacy" />
                        </ListItemLink>
                        <ListItemLink href="/sell">
                            <ListItemText primary="Sell with Splintr" />
                        </ListItemLink>
                        <ListItemLink href="/payment">
                            <ListItemText primary="Payment Methods" />
                        </ListItemLink>
                        <ListItemLink href="/contact">
                            <ListItemText primary="Contact Us" />
                        </ListItemLink>
                    </List>
                </Grid>
                <Grid item xs={6} sm={6} md={6}>
                    <List component="nav">
                        <ListItemLink href="/buy">
                            <ListItemText primary="Buy Now, Pay Later" />
                        </ListItemLink>
                        <ListItemLink href="/customer">
                            <ListItemText primary="Customer Service" />
                        </ListItemLink>
                        <ListItemLink href="/partners">
                            <ListItemText primary="Partners" />
                        </ListItemLink>
                    </List>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item xs={12} sm={12} md={12}>
                    <List className={classes.flexContainer}>
                        <Link target="_blank" href="https://www.facebook.com">
                            <img alt="facebook" src="https://res.cloudinary.com/djpepozcx/image/upload/v1615034916/splintr/facebook-circle-line.svg" />
                        </Link>
                        <Link target="_blank" href="https://www.instagram.com">
                            <img alt="instagram" src="https://res.cloudinary.com/djpepozcx/image/upload/v1615034916/splintr/instagram-line.svg" />
                        </Link>
                        <Link target="_blank" href="https://www.twitter.com">
                            <img alt="twitter" src="https://res.cloudinary.com/djpepozcx/image/upload/v1615034916/splintr/twitter-line.svg" />
                        </Link>
                        <Link target="_blank" href="https://www.linkedin.com">
                            <img alt="linkedin" src="https://res.cloudinary.com/djpepozcx/image/upload/v1615034916/splintr/linkedin-box-line.svg" />
                        </Link>
                    </List>
                </Grid>
            </Grid>
        </footer>
    );
}
