import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

export default function Learnmoreblock() {
    const useStyles = makeStyles((theme) => ({
        learnblock: {
            backgroundColor: "#47DAEE", color: "#fff", paddingLeft: "150px", paddingTop: "50px", paddingBottom: "50px",
            "@media (max-width: 768px)": { paddingLeft: "30px", paddingRight: "30px"}
        },
        moreBtn: {
            backgroundColor: "#000", color: "#fff", textTransform: "none", minWidth: "150px", borderRadius: "30px"
        },
        textBold: {
            fontWeight: "bold"
        }
	}));

	const classes = useStyles();
    return (
        <Box mt={16} className={classes.learnblock}>
            <Typography variant="h4" className={classes.textBold}>
                Calling out all merchants!
            </Typography>
            <Box mt={2} mb={2}>
                <Typography variant="subtitle1">
                    We're partnering retailers like you!
                </Typography>
            </Box>
            <Box mt={2} mb={2}>
                <Typography variant="subtitle1">
                    Team up with us to give you customer a better way to pay. Shoppers love the flexibility <br/>
                    and confidence that your solutions give them and our integration options make it easy for <br/>
                    you to add Splintr to yor checkout.
                </Typography>
            </Box>
            <Button className={classes.moreBtn}>Learn More</Button>
        </Box>
    );
}
