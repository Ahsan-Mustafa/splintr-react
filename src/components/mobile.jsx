import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

export default function Mobileblock() {
    const useStyles = makeStyles((theme) => ({
        mobileBanner: { maxHeight: "100%", float: "right" },
        imgResponsive: { maxWidth: '100%', display: 'block' },
        textBold: { fontWeight: 'bold' },
        qrimg: { maxWidth: "80%" },
        Mobileblock: { marginTop: "50px", marginBottom: "100px" },
        qrtext: { "@media (max-width: 768px)": { fontSize: "13px" }},
        textinfo: {
            '& h3': { "@media (max-width: 768px)": { fontSize: "28px" }   },
            '& h6': { "@media (max-width: 768px)": { fontSize: "14px" }   },
        }
	}));

	const classes = useStyles();
    return (
        <Grid container className={classes.Mobileblock}>
			<Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
				<img className={`${classes.imgResponsive} ${classes.mobileBanner}`} src="https://res.cloudinary.com/djpepozcx/image/upload/v1614731648/splintr/phone.png" alt="mobileBanner"/>
			</Grid>
			<Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                <Box mt={10} className={classes.textinfo}>
                    <Typography variant="h3" className={classes.textBold}>
                        Shop Now, <br/> Pay Later!
                    </Typography>
                    <Box mt={4} mb={4}>
                        <Typography variant="h6" display="block">
                            Our flexible options allow you to choose<br/> 
                            between paying after delivery or paying in <br/>
                            over 3,4 or 6 monthly installments.
                        </Typography>
                    </Box>
                    <Box mt={2} mb={2}>
                        <Typography variant="h6" display="block" className={classes.textBold}>
                            Don't forget to download the app!
                        </Typography>
                    </Box>
                    <Grid container>
                        <Grid item xs={6} sm={3} md={3}>
                            <Box borderRight={1}>
                                <img className={classes.qrimg} src="https://res.cloudinary.com/djpepozcx/image/upload/v1614731648/splintr/image_1.png" alt="qrcode"/>
                            </Box>
                        </Grid>
                        <Grid item xs={6} sm={9} md={9}>
                            <Box ml={3}>
                                <Typography className={classes.qrtext}>
                                    Scan the QR code<br/> and download the <br/> app to enjoy our <br/> exclusive shopping <br/> experiences!
                                </Typography>
                            </Box>
                        </Grid>
                    </Grid>
                </Box>
			</Grid>
		</Grid>
    );
}
