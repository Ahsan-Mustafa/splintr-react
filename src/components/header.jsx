import {AppBar, Toolbar, makeStyles, Button, IconButton, Drawer, Link, MenuItem, Box } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import React, { useState, useEffect } from "react";
import { Link as RouterLink } from "react-router-dom";
import Container from '@material-ui/core/Container';

const headersData = [
  { label: "Splintr for business", href: "https://splintr.xyz/#Services" },
  { label: "How it works", href: "https://splintr.xyz/#How-it-work" },
  { label: "Splintr Stores", href: "https://splintr.xyz/#Sell" },
];

const useStyles = makeStyles(() => ({
  header: {
    backgroundColor: "transparent", background: "linear-gradient(90deg, rgb(254 171 176) 0%, rgb(255 184 186) 50%, rgb(254 188 190) 100%)",
    boxShadow: "none", WebkitBoxShadow: "none", MozBoxShadow:"none", position: "relative",
    "@media (max-width: 900px)": { paddingLeft: 0 },
  },
  menuButton: { fontFamily: "Open Sans, sans-serif", fontWeight: 700, size: "16px", marginLeft: "30px", color: "#000", textTransform:"none"},
  toolbar: { display: "flex", justifyContent: "space-between",
    "@media (max-width: 768px)": { paddingLeft: "0px", paddingRight: "0px"}
  },

  headerBtn: {
    backgroundColor: "#000", color: "#fff", borderRadius: "50px", marginLeft: "10px", textTransform: "none", minWidth: "130px", fontSize: "13px",
    "&:hover": { backgroundColor: "#000", color: "#fff"},
    "@media (max-width: 768px)": { minWidth: "auto", fontSize: "11px"}
  },
  Pad5: {
    paddingLeft: "5px", paddingRight: "5px",
    "&>div" : { paddingLeft: "0px", paddingRight: "0px" },
    "@media (max-width: 768px)": { "&>div>div>div" : { paddingLeft: "0px", paddingRight: "0px" }}
	}
}));

export default function Header() {
  const { header, menuButton, toolbar,  headerBtn, Pad5 } = useStyles();

  const [state, setState] = useState({
    mobileView: false,
    drawerOpen: false,
  });

  const { mobileView, drawerOpen } = state;

  useEffect(() => {
    const setResponsiveness = () => {
      return window.innerWidth < 900
        ? setState((prevState) => ({ ...prevState, mobileView: true }))
        : setState((prevState) => ({ ...prevState, mobileView: false }));
    };

    setResponsiveness();

    window.addEventListener("resize", () => setResponsiveness());
  }, []);

  const displayDesktop = () => {
    return (
      <Toolbar className={toolbar}>
        {splintrLogo}
        <div>{getMenuButtons()}</div>
      </Toolbar>
    );
  };

  const displayMobile = () => {
    const handleDrawerOpen = () =>
      setState((prevState) => ({ ...prevState, drawerOpen: true }));
    const handleDrawerClose = () =>
      setState((prevState) => ({ ...prevState, drawerOpen: false }));

    return (
      <Toolbar>
        <IconButton {...{ edge: "start", color: "inherit", "aria-label": "menu", "aria-haspopup": "true", onClick: handleDrawerOpen, }}>
          <MenuIcon />
        </IconButton>

        <Drawer {...{ anchor: "left", open: drawerOpen, onClose: handleDrawerClose,}}>
          <div>{getDrawerChoices()}</div>
        </Drawer>

        <div>{splintrLogo}</div>
      </Toolbar>
    );
  };

  const getDrawerChoices = () => {
    return headersData.map(({ label, href }) => {
      return (
        <Link {...{ 
              component: RouterLink,
              to: href,
              color: "inherit",
              style: { 
                textDecoration: "none",
              }, key: label
            }} >
          <MenuItem>{label}</MenuItem>
        </Link>
      );
    });
  };

  const splintrLogo = (
    <img src="https://res.cloudinary.com/djpepozcx/image/upload/v1614731649/splintr/logo.svg" alt="logo"/>
  );

  const getMenuButtons = () => {
    return headersData.map(({ label, href }) => {
      return (
        <Button {...{ key: label, color: "inherit", to: href, target: "_blank", component: RouterLink, className: menuButton }}>
          {label}
        </Button>
      );
    });
  };

  return (
    <AppBar className={header}>
      <Container maxWidth="lg" className={Pad5}>
        <Toolbar>
          <Box display="flex" flexGrow={1}>
            {mobileView ? displayMobile() : displayDesktop()}
          </Box>
          <Button size="medium" className={headerBtn}>
            Get the app
          </Button>
          <Button component={RouterLink} to={'/signin'} size="medium" className={headerBtn}>
            Login
          </Button>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
