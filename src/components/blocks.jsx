import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

export default function Blocks() {
    const useStyles = makeStyles((theme) => ({
        infoblock: {
            marginTop: "80px",
            marginBottom: "80px"
        },
        textbold: {
            fontWeight: "bold"
        },
        subtitleText: {
            fontSize: "17px",
            lineHeight: "1.4"
        }
	}));

	const classes = useStyles();
    return (
        <Grid container className={classes.infoblock}>
			<Grid item xs={12} sm={4} md={4} lg={4} xl={4}>
				<img src="https://res.cloudinary.com/djpepozcx/image/upload/v1615034916/splintr/shopping-cart-2-fill.svg" alt="shopping"/>
				<Typography className={classes.textbold} variant="h5" component="h2">Buy whatever <br/> you want</Typography>
				<Box mt={4} mb={4}>
					<Typography className={classes.subtitleText} display="block">
						Enjoy buying from whatever<br/> store you'd like - just don't <br/>forget to choose us at checkpoint.
					</Typography>
				</Box>
			</Grid>
			<Grid item xs={12} sm={4} md={4} lg={4} xl={4}>
				<img src="https://res.cloudinary.com/djpepozcx/image/upload/v1615034916/splintr/bank-card-2-fill.svg" alt="card"/>
				<Typography className={classes.textbold} variant="h5" component="h2">No Interest or <br/> late payment fees</Typography>
				<Box mt={4} mb={4}>
					<Typography className={classes.subtitleText} display="block">
						Imagining paying whenever <br/> you want without having to pay <br/> interest free or any extras?
					</Typography>
				</Box>
			</Grid>
			<Grid item xs={12} sm={4} md={4} lg={4} xl={4}>
				<img src="https://res.cloudinary.com/djpepozcx/image/upload/v1615034918/splintr/coins-line.svg" alt="coins"/>
				<Typography className={classes.textbold} variant="h5" component="h2">Pay in 3,4 or 6 <br/> installments</Typography>
				<Box mt={4} mb={4}>
					<Typography className={classes.subtitleText} display="block">
						You can choose to pay how <br/> you'd like without our installment <br/> plans that go up to 6 months!   
					</Typography>
				</Box>
			</Grid>
		</Grid>
    );
}
