import React from "react";
import {  withStyles } from '@material-ui/core/styles';
import Whirligig from "react-whirligig";
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';


let whirligig
const next = () => whirligig.next()
const prev = () => whirligig.prev()

const useStyles = (theme) => ({
    whirlbox: {
        "& > div": { marginTop: "20px",marginBottom: "40px", "&::-webkit-scrollbar": { display: "none" }},
        "& img": { maxWidth: "200px"}
    },
    textBold: {
        fontWeight: 'bold'
    }
});

class Deals extends React.Component {
    render() {
        const { classes } = this.props;
        return (
            <Box>
                <Box className={classes.whirlbox}>
                    <Typography variant="h6" className={classes.textBold}>
                        {this.props.title}
                    </Typography>
                    <Button onClick={prev}>
                        <img src="https://res.cloudinary.com/djpepozcx/image/upload/v1615034918/splintr/arrow-left-s-line.svg" alt="arrow"/>
                    </Button> 
                    <Button onClick={next}>
                        <img src="https://res.cloudinary.com/djpepozcx/image/upload/v1615034918/splintr/arrow-right-s-line.svg" alt="arrow"/>
                    </Button>
                    <Whirligig gutter="1em" slideBy={2} ref={(_whirligigInstance) => { whirligig = _whirligigInstance}}>
                        {
                            this.props.data.map(function(list){
                                return(
                                    <Link href={list.website}>
                                        <img alt={list.name} src={list.bg_image}/>
                                    </Link>
                                )
                            })
                        }
                    </Whirligig>
                </Box>
            </Box>
            
        );
    }
}

export default withStyles(useStyles)(Deals);