const best_sellers = [
	{"id": 1,"name": "Faces","website": "http://faces.com","bg_image": "https://res.cloudinary.com/djpepozcx/image/upload/v1614731692/splintr/faces.png",},
    {"id": 2,"name": "Serena williams","website": "https://www.serenawilliams.com/","bg_image": "https://res.cloudinary.com/djpepozcx/image/upload/v1614731693/splintr/s.png",},
    {"id": 3,"name": "H&M","website": "https://www.hm.com/","bg_image": "https://res.cloudinary.com/djpepozcx/image/upload/v1614731692/splintr/h_m.png",},
    {"id": 4,"name": "Champs Sports","website": "https://www.champssports.com/","bg_image": "https://res.cloudinary.com/djpepozcx/image/upload/v1614731691/splintr/champs.png",},
    {"id": 5,"name": "Adidas","website": "https://www.adidas.com/us","bg_image": "https://res.cloudinary.com/djpepozcx/image/upload/v1614731691/splintr/adidas.png",},
	{"id": 6,"name": "Haus Laboratories","website": "https://www.hauslabs.com/","bg_image": "https://res.cloudinary.com/djpepozcx/image/upload/v1614731692/splintr/haus.png",},
    {"id": 7,"name": "Sephora","website": "https://www.sephora.com/","bg_image": "https://res.cloudinary.com/djpepozcx/image/upload/v1614731693/splintr/sephora.png",}
];

const top_deals = [

	{"id": 9,"name": "SHE IN","website": "https://us.shein.com/","bg_image": "https://res.cloudinary.com/djpepozcx/image/upload/v1614731691/splintr/shein.png",},
    {"id": 10,"name": "Pandora","website": "https://us.pandora.net/","bg_image": "https://res.cloudinary.com/djpepozcx/image/upload/v1614731692/splintr/pandora.png",},
    {"id": 11,"name": "MCM","website": "https://www.mcmworldwide.com/","bg_image": "https://res.cloudinary.com/djpepozcx/image/upload/v1614731693/splintr/mcm.png",},
    {"id": 12,"name": "H&M","website": "https://www.hm.com/","bg_image": "https://res.cloudinary.com/djpepozcx/image/upload/v1614731692/splintr/h_m.png",},
    {"id": 13,"name": "Champs Sports","website": "https://www.champssports.com/","bg_image": "https://res.cloudinary.com/djpepozcx/image/upload/v1614731691/splintr/champs.png",},
    {"id": 14,"name": "rue 21","website": "https://www.rue21.com/store/","bg_image": "https://res.cloudinary.com/djpepozcx/image/upload/v1614731692/splintr/rule-21.png",}
]

export {
    best_sellers, top_deals
}